package com.apitechu.oracle.model;

import org.springframework.data.annotation.Id;

import javax.persistence.*;

@Entity
@Table(name="PRODUCTOS")
public class ProductoModel {
    @Column(name="ID")
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private String id;

    @Column(name = "DESCRIPCION")
    private String descripcion;

    @Column(name="PRECIO")
    private double precio;

    public ProductoModel(){
    }

    public ProductoModel(String id, String descripcion, double precio){
        this.id = id;
        this.descripcion = descripcion;
        this.precio = precio;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

}
